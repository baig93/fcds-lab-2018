#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/sysinfo.h>
#include <assert.h>

#define NUM_THREADS 10

int nNodes, loopSize;
short int* graph;

long long cnt;

void* warshall(void*);
void read();
void write(FILE *fl);

int min(int a, int b)
{
	return (a < b) ? a : b;
}

void write(FILE *fl)
{
	int i, j;

	for(i = 0; i < nNodes; i++)
	{
		for(j = 0; j < nNodes; j++)
			fprintf(fl, "%d ", graph[i * nNodes + j]);

		fprintf(fl, "\n");
	}
}

void read ()
{

	char line[50];
	char* token;
	int size = 50;

	int l;
	int c;

	while(!feof(stdin))
	{

		

		if(fgets(line, size, stdin) == NULL) break; //read line until

		token = strtok(line, " "); // split using space as divider

		if(*token == 'p')
		{

			token = strtok(NULL, " "); // sp

			token = strtok(NULL, " "); // no. of vertices
			nNodes = atoi(token);

			loopSize = (nNodes + NUM_THREADS - 1) / NUM_THREADS; //loop size for threads

			token = strtok(NULL," "); // no. of directed edges

			graph = (short int*) calloc(nNodes * nNodes, sizeof (short int)); //give dimensions and initialize to zero

			if (graph == NULL)
			{
				printf( "Error in graph allocation: NULL!\n");
				exit( EXIT_FAILURE);
			}

		}
		else if(*token == 'a')
		{
			token = strtok(NULL, " ");
			l = atoi(token) - 1;

			token = strtok(NULL, " ");
			c = atoi(token) - 1;

			token = strtok(NULL, " "); //weight
			graph[l * nNodes + c] = 1;
		}

	}

}


void* warshall(void* arg)
{
  int st = *((int*) arg); //get the value of start iteration for loop

	for (int k = 0; k < min(nNodes, st + loopSize); k++)
	{
		for (int i = 0; i < nNodes; i++)
		{
			for (int j = 0; j < nNodes; j++)
			{
				if(graph[i * nNodes + k] + graph[k * nNodes + j] == 2 && i != j)
					graph[i * nNodes + j] = 1;

			}
		}
	}

}

int main(int argc, char *argv[] )
{
	int cpus = get_nprocs();
	if (getenv("MAX_CPUS")) 
	{
		cpus = atoi(getenv("MAX_CPUS"));
	}
	assert(cpus > 0 && cpus <= 64);
	fprintf(stderr, "Running on %d CPUS\n", cpus);

	read();

	int st[NUM_THREADS];
	pthread_t tid[NUM_THREADS];

	for(int i = 0; i < NUM_THREADS; i++)
    	st[i] = i * NUM_THREADS;

  //create and start thread with pre-calculated value of loop
  for(int i = 0; i < NUM_THREADS; i++)
    pthread_create(&tid[i], NULL, warshall, st + i);

    //wait for threads to finish
  for(int i = 0; i < NUM_THREADS; i++)
    pthread_join(tid[i], NULL);

	write(stdout);

	free(graph);

	return 0;
}
