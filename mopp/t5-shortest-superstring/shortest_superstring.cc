#include <iostream>
#include <set>
#include <string>
#include <vector>
#include <algorithm>
#include <utility>
#include <sys/sysinfo.h>
#include <assert.h>
#include <pthread.h>


using namespace std;

set <string> s;
int threadNum;
int maxOverlapValue = -1;
pair <string, string> highestOverlapPair;


bool is_prefix (string a, string b)
{
    if (a.size() > b.size()) return false ;

    if (!(std::mismatch(a.begin(), a.end(), b.begin()).first == a.end ()))
        return false;

    return true ;
}

string suffix_from_position (string x, int i)
{
    return x.substr(i) ;
}

string remove_prefix (string x, int n)
{
    if (x.size() > n)
        return suffix_from_position (x, n);

    return x;
}

set <string> all_suffixes (string x)
{
    set <string> ss;

    int n = x.size();

    while (-- n) {
        ss.insert(x.substr(n)) ;
    }

    return ss;
}

string commom_suffix_and_prefix (string a, string b)
{
    if (!a.size()) return "" ;
    if (!b.size()) return "" ;

    string x = "" ;

    set <string> suffixes = all_suffixes(a);
    set <string> :: iterator it;

    for (it = suffixes.begin(); it != suffixes.end(); it++) {
        if (is_prefix(*it, b) && (*it).size() > x.size()) {
            x = *it ;
        }
    }
    return x ;
}

string overlap (string s1, string s2)
{
    string c = commom_suffix_and_prefix (s1, s2) ;
    return s1 + remove_prefix (s2, c.size()) ;
}

void pop_two_elements_and_push_overlap()
{
  string super = overlap(highestOverlapPair.first, highestOverlapPair.second);

  s.erase(s.find(highestOverlapPair.first));
  s.erase(s.find(highestOverlapPair.second));

  s.insert(super);

  maxOverlapValue = -1;
}

int overlap_value(string s1, string s2)
{
  return (commom_suffix_and_prefix (s1, s2)).size();
}

void* highest_overlap_value(void* arg)
{
  string st = *((string*) arg);

  set <string> :: iterator it;

  for(it = s.begin(); it != s.end(); it++)
  {
    if(st != (*it)) //not the same strings
    {
      if(maxOverlapValue < overlap_value(st, *it))
      {
        maxOverlapValue = overlap_value(st, *it);
        highestOverlapPair = make_pair(st, *it);
      }
    }
  }
}

void set_to_vector(vector <string> *v)
{
  set <string> :: iterator it;

  (*v).resize(0);

  for(it = s.begin(); it != s.end(); it++)
    (*v).push_back(*it);
}

string shortest_superstring()
{
  if(s.empty()) return "";

  vector <string> threadString;

  while((threadNum = s.size()) > 1) //while there are at least two elements in set
  {
    set_to_vector(&threadString); //for thread argument fill vector with strings of set
	if(threadNum=2)
	{	threadNum = 2;	}
	
    pthread_t td[threadNum];// create thread array

    //create threads and pass String as argument, in order to find highest overlap value
    for(int i = 0; i < threadNum; i++)
      pthread_create(&td[i], NULL, highest_overlap_value, &threadString[i]);

    //wait for threads to find highest overlap pair
    for(int i = 0; i < threadNum; i++)
      pthread_join(td[i], NULL);

    pop_two_elements_and_push_overlap(); //find overlap string and put it into set
  }

  return *(s.begin()); //return 1st string (which is superstring)
}

set <string> read_strings_from_standard_input()
{
  string st;
  int n;
  set <string> temp;

  cin >> n; //get number of strings

  while(n--)
  {
    cin >> st;
    temp.insert(st); //collect into set
  }

  return temp;
}

int main()
{
	int cpus = get_nprocs();
	if (getenv("MAX_CPUS"))		//getting MAX number of CPUS
  	{
    		cpus = atoi(getenv("MAX_CPUS"));
  	}

    	assert(cpus > 0 && cpus <= 64);
    	fprintf(stderr, "Running on %d CPUS\n", cpus);

  s = read_strings_from_standard_input();

  string ans = shortest_superstring();

  cout << ans << endl;

  return 0;
}

