/*
* Checker for t5 shortest superstring judge file
* compile as: g++ -std=c++11 checker.cc -o judge_checker
* 
* run command: ./judge_checker < judge.in
*/
#include <iostream>
#include <fstream>

#include <string.h>
using namespace std;

int lines;
int main(int argc, char **argv) {
// read ***.in file
	char buf[260];
	lines=std::stoi(fgets(buf,260,stdin));
	string elements[lines];
	int i=0;
	while(fgets(buf,260,stdin)){
		strtok(buf,"\n");
		elements[i]=buf;
		cout<<elements[i]<<endl;
		i++;
	}
//
	cout <<endl;
//  read ***.out
	ifstream f;
	f.open("my_judge.out");
	string buff;
	if(f.is_open()){
		f>>buff;
		cout<< buff<<endl;
		
	}
	f.close();
//
// chech if everything is inside the shortest super string
	int found=0,not_found=0;
	for(i=0;i<lines;i++){
		if( buff.find(elements[i]) != std::string::npos){
			cout<<"FOUND : "<<elements[i]<<endl;
			found++;
		}else{
			cout<<"NOT FOUND : "<<elements[i]<<endl;
			not_found++;
		}
	}
	cout<<"Length of shortest super string: "<<buff.length()<<endl;
	cout<<"FOUND="<<found<<endl<<"NOT FOUND="<<not_found<<endl;
	return 0;
}

